FROM python:3.8.3

WORKDIR /usr/src/app
COPY ./app .

RUN apt-get update && apt-get install -y \
    supervisor \
    vim \
    python3-pip

RUN pip install --no-cache-dir --upgrade pip setuptools && \
    pip install --no-cache-dir --upgrade pipenv

RUN echo 'alias runner="/usr/src/app/console/runner"' >> ~/.bashrc && \
    echo 'export PIPENV_VENV_IN_PROJECT=1' >> ~/.bashrc

RUN /usr/src/app/console/runner sync --clear

COPY ./docker-entrypoint.sh /

RUN chmod +x /docker-entrypoint.sh

ENTRYPOINT ["/bin/bash", "-c", ". /docker-entrypoint.sh"]
