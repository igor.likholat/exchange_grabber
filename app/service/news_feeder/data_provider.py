import os
from abc import ABC

from service.news_feeder.model import KeywordCollection, Keyword, TickerProviderFileConfig


class DataProviderAbstract(ABC):
    def get_keywords(self) -> KeywordCollection:
        raise NotImplementedError()


class DataProviderFile(DataProviderAbstract):
    def __init__(self, config: TickerProviderFileConfig):
        self.__file_path = self.__get_path(config.file_path)

    def get_keywords(self) -> KeywordCollection:
        collection = KeywordCollection()
        with open(self.__file_path) as f:
            keywords = f.readlines()
            [collection.add(Keyword(keyword.strip())) for keyword in keywords]
        return collection

    @staticmethod
    def __get_path(relative_path: str) -> str:
        script_dir = os.path.dirname(__file__)
        return os.path.join(script_dir, relative_path)
