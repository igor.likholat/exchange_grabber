from service.news_feeder import DataProviderAbstract
from service.sender import Telega


class Trends:
    def __init__(self, data_provider: DataProviderAbstract, sender: Telega):
        self.__data_provider = data_provider
        self.__sender = sender

    def analyze(self):
        for keyword in self.__data_provider.get_keywords():
            self.__sender.send(keyword.value)
