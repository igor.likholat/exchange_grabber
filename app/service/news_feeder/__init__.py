from service.news_feeder.data_provider import DataProviderAbstract, DataProviderFile
from service.news_feeder.model import TickerProviderFileConfig


def create_news_feeder_ticker_provider_file(config: TickerProviderFileConfig) -> DataProviderAbstract:
    return DataProviderFile(config=config)
