from dataclasses import dataclass


@dataclass(frozen=True)
class TickerProviderFileConfig:
    file_path: str


@dataclass(frozen=True)
class Keyword:
    value: str


class KeywordCollection:
    def __init__(self, keywords: [Keyword] = ()):
        self.__items = []
        [self.add(keyword) for keyword in keywords]

    def add(self, result: Keyword) -> None:
        self.__items.append(result)

    def __iter__(self) -> [Keyword]:
        return iter(self.__items)
