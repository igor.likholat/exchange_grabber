from dataclasses import dataclass
import asyncio
import aiomysql


@dataclass(frozen=True)
class Config:
    host: str
    port: int
    user: str
    password: str
    db: str


class ConnectionProvider:
    def __init__(self, config: Config):
        self.__config = config

    async def create_connection(self):
        return await aiomysql.connect(
            host=self.__config.host,
            port=self.__config.port,
            user=self.__config.user,
            password=self.__config.password,
            db=self.__config.db,
            # todo move to settings
            unix_socket='/tmp/mysqld.sock'
        )
