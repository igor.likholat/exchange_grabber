from pytrends.request import TrendReq


pytrends = TrendReq(hl='en-US')

cat = '0'
timeframes = ['today 5-y']
geo = ''
gprop = ''

kw_list = ['acbi',]
keywords = []


def check_trends():
    pytrends.build_payload(keywords,
                           cat=cat,
                           timeframe=timeframes[0],
                           geo=geo,
                           gprop=gprop)

    data = pytrends.interest_over_time()
    print(data)

    # среднее за последние 5 лет
    # показывает стабильность тренда (во времени), относительно пика. Чем выше - тем стабильнее
    mean = round(data.mean(), 2)
    print(mean)

    # среднее за последний год (-52 недели от сегодня)
    avg = round(data[kw][-52:].mean(), 2)
    print(avg)
    exit()

    # среднее за последние 5 лет
    avg2 = round(data[kw][:52].mean(), 2)

    # соотношение последнего года к глобальному тренду (5 лет)
    trend = round(((avg / mean[kw]) - 1) * 100, 2)

    # как соотносится последний год со средним 5 лет
    trend2 = round(((avg / avg2) - 1) * 100, 2)

    print('The average 5 years interest of "' + kw + '" was ' + str(mean[kw]) + '.')
    print('The last year interest of "' + kw + '" compared to the last 5 years '
          + 'has changed by ' + str(trend) + '%.')

    # Stable trend
    if mean[kw] > 75 and abs(trend) <= 5:
        print('The interest for "' + kw + '" is stable in the last 5 years')
    elif mean[kw] > 75 and trend > 5:
        print('The interest for "' + kw + '" is stable and increasing in the last 5 years')
    elif mean[kw] > 75 and trend < -5:
        print('The interest for "' + kw + '" is stable and decreasing in the last 5 years')

    # Relatively stable trend
    elif mean[kw] > 60 and abs(trend) < 15:
        print('The interest for "' + kw + '" is relatively stable in the last 5 years')
    elif mean[kw] > 60 and trend > 15:
        print('The interest for "' + kw + '" is relatively stable and increasing in the last 5 years')
    elif mean[kw] > 60 and trend < -15:
        print('The interest for "' + kw + '" is relatively stable and decreasing in the last 5 years')

    # Seasonal trend
    elif mean[kw] > 20 and abs(trend) <= 15:
        print('The interest for "' + kw + '" is seasonal')

    # New keyword
    elif mean[kw] > 20 and trend > 15:
        print('The interest for "' + kw + '" is trending')

    # Declining keyword
    elif mean[kw] > 20 and trend < -15:
        print('The interest for "' + kw + '" is significantly decreasing')

    # Cyclical
    elif mean[kw] > 20 and abs(trend) <= 15:
        print('The interest for "' + kw + '" is cyclical')

    # New
    elif mean[kw] > 0 and trend > 15:
        print('The interest for "' + kw + '" is new and trending')

    # Declining
    elif mean[kw] > 0 and trend < -15:
        print('The interest for "' + kw + '" is declining and not comparable to its peak')

    # Other
    else:
        print('This is something to be checked')

    # Comparison last year vs. 5 years ago
    if avg2 == 0:
        print('This did not exist 5 years ago')
    elif trend2 > 15:
        print('The last year interest is quite higher compared to 5 years ago '
              + 'it has increased by ' + str(trend2) + '%.')
    elif trend2 < -15:
        print('The last year interest is quite lower compared to 5 years ago '
              + 'it has decreased by ' + str(trend2) + '%.')
    else:
        print('The last year interest is comparable to 5 years ago '
              + 'it has changed by ' + str(trend2) + '%.')

    print('')


for kw in kw_list:
    keywords.append(kw)
    check_trends()
    keywords.pop()

