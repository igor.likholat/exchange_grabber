import asyncio
import inject
from async_cron.job import CronJob
from async_cron.schedule import Scheduler
from di.services import services


if __name__ == "__main__":
    inject.configure(services)
    reader = inject.instance('option_quote_reader')

    msh = Scheduler(locale="en_US")
    myjob = CronJob(name='option_quote_reader').every(180).second.go(reader.read_data)
    msh.add_job(myjob)
    loop = asyncio.get_event_loop()

    try:
        loop.run_until_complete(msh.start())
    except KeyboardInterrupt:
        print('exit')
