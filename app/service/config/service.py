from service.config.loader import ConfigLoaderAbstract
from service.mysql.connection_provider import Config as MysqlConfig
from service.news_feeder.model import TickerProviderFileConfig
from service.rabbitmq.connection_provider import Config as RabbitmqConfig
from service.sender.model import TelegaConfig


class ConfigService:
    def __init__(self, config_loader: ConfigLoaderAbstract):
        self.__config_loader = config_loader
        self.__config = None

    def get_config_mysql(self) -> MysqlConfig:
        config = self.config['Mysql']
        return MysqlConfig(
            host=config.get('host'),
            port=config.get('port'),
            user=config.get('user'),
            password=config.get('password'),
            db=config.get('db'),
        )

    def get_config_rabbitmq(self) -> RabbitmqConfig:
        config = self.config['Rabbitmq']
        return RabbitmqConfig(
            host=config.get('host'),
            login=config.get('login'),
            password=config.get('password'),
            vhost=config.get('vhost'),
            port=config.get('port'),
        )

    def get_config_telegram(self) -> TelegaConfig:
        config = self.config['Telega']
        return TelegaConfig(
            token=config.get('token'),
            channel=config.get('channel'),
        )

    def get_config_file_ticker_provider(self) -> TickerProviderFileConfig:
        config = self.config['TickerProviderFile']
        return TickerProviderFileConfig(
            file_path=config.get('file_path')
        )

    @property
    def config(self):
        if self.__config is None:
            self.__config = self.__config_loader.load_config()
        return self.__config
