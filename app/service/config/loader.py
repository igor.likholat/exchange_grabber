from abc import ABC


class ConfigLoaderAbstract(ABC):
    def load_config(self) -> dict:
        raise NotImplemented()


class ConfigLoaderDict(ConfigLoaderAbstract):
    def __init__(self, config: dict):
        self.__config = config

    def load_config(self) -> dict:
        return self.__config
