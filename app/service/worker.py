from abc import ABC
import asyncio
import signal


class WorkerAbstract(ABC):
    async def before_start(self, **kwargs) -> None:
        raise NotImplementedError()

    async def handle_tick(self) -> None:
        raise NotImplementedError()

    async def stop(self) -> None:
        raise NotImplementedError()


class Worker:
    ARG_MIN_TICK_TIME_SEC = 'min_tick_time_sec'
    SLEEP_INTERVAL = 0.1

    def __init__(
            self,
            service: WorkerAbstract,
            min_tick_time_sec: int or None = None
    ):
        signal.signal(signal.SIGINT, self.stop)
        signal.signal(signal.SIGTERM, self.stop)
        signal.signal(signal.SIGTSTP, self.stop)
        signal.signal(signal.SIGUSR1, self.stop)
        self.__service = service
        self.__min_tick_time_sec = min_tick_time_sec
        self.__force_stop = False

    def handle(self, **kwargs) -> None:
        self.__min_tick_time_sec = kwargs.get(self.ARG_MIN_TICK_TIME_SEC, self.__min_tick_time_sec)
        asyncio.run(self.__run(**kwargs))

    def stop(self, num, frame):
        self.__force_stop = True

    async def __run(self, **kwargs) -> None:
        await self.__service.before_start(**kwargs)
        try:
            while not self.__force_stop:
                start_time = asyncio.get_running_loop().time()
                await self.__service.handle_tick()
                end_time = asyncio.get_running_loop().time()
                await self.__sleep_if_need(end_time - start_time)
        finally:
            await self.__service.stop()

    async def __sleep_if_need(self, last_tick_duration: float) -> None:
        if self.__min_tick_time_sec is not None:
            sleep_left = self.__min_tick_time_sec - last_tick_duration
            while sleep_left > 0 and not self.__force_stop:
                sleep_duration = min(sleep_left, self.SLEEP_INTERVAL)
                await asyncio.sleep(sleep_duration)
                sleep_left -= self.SLEEP_INTERVAL
