import asyncio
import aio_pika
import json
from abc import ABC
from logging import Logger
from service.broker.exante.model import OptionRequest as ExanteOptionRequest
from service.option_grabber.data_saver import DataSaverAbstract
from service.option_grabber.model import Instrument, InstrumentQuote
from service.rabbitmq.connection_provider import ConnectionProvider


class OptionConsumerProcessorAbstract(ABC):
    async def take_data(self, option: Instrument) -> None:
        raise NotImplementedError()


class Consumer:
    def __init__(
            self,
            rabbitmq: ConnectionProvider,
            option_consumer: OptionConsumerProcessorAbstract,
            queue_name: str,
            thread_num: int,
            logger: Logger,
    ):
        self.__rabbitmq = rabbitmq
        self.__option_consumer = option_consumer
        self.__queue_name = queue_name
        self.__thread_num = thread_num
        self.__logger = logger

    async def run(self, loop):
        connection = await self.__rabbitmq.create_connection(loop)
        channel = await connection.channel()
        await channel.set_qos(prefetch_count=self.__thread_num)

        queue = await channel.declare_queue(self.__queue_name, auto_delete=True)
        await queue.consume(self.__process_message)
        return connection

    async def __process_message(self, message: aio_pika.IncomingMessage):
        async with message.process():
            option_dict = json.loads(message.body)
            await self.__option_consumer.take_data(Instrument(
                id=option_dict.get('id'),
                symbol=option_dict.get('symbol'),
            ))


class OptionConsumerProcessorExante(OptionConsumerProcessorAbstract):
    # client should be service.broker.exante.client.ClientAbstract
    def __init__(
            self,
            data_saver: DataSaverAbstract,
            client,
            logger: Logger
    ):
        self.__data_saver = data_saver
        self.__client = client
        self.__logger = logger

    async def take_data(self, option: Instrument) -> None:
        response = await self.__client.get_price(request=ExanteOptionRequest(
            id=option.id,
            symbol=option.symbol
        ))
        if response is not None:
            await self.__data_saver.save_price(InstrumentQuote(
                id=response.option_id,
                tracked_at=response.tracked_at,
                bid=response.bid,
                bid_volume=response.bid_volume,
                ask=response.ask,
                ask_volume=response.ask_volume,
            ))
