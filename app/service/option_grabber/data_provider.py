from abc import ABC
import datetime

from service.mysql.connection_provider import ConnectionProvider
from service.option_grabber.model import InstrumentCollection, Instrument, InstrumentQuoteCollection, InstrumentQuote


class DataProviderAbstract(ABC):
    async def get_data(self) -> InstrumentCollection:
        raise NotImplementedError()

    async def get_quote_data(self, date_from: str, date_to: str) -> InstrumentQuoteCollection:
        raise NotImplementedError()


class OptionProviderMysql(DataProviderAbstract):
    def __init__(self, mysql: ConnectionProvider):
        self.__mysql = mysql
        self.__connection = None

    async def get_data(self) -> InstrumentCollection:
        now = datetime.datetime.now()
        now.replace(tzinfo=datetime.timezone.utc)
        date_to = datetime.datetime(now.year, now.month, now.day, 23, 59, 59, 0)

        collection = InstrumentCollection()
        connection = await self.__get_connection()
        cursor = await connection.cursor()
        await cursor.execute("""
            SELECT
                id,
                symbol,
                asset_id
            FROM
                option_data
            WHERE
                is_active = 1
                AND expiration >= %s
        """, date_to)

        [
            collection.add(Instrument(id=option[0], symbol=option[1], parent_id=option[2]))
            for option in await cursor.fetchall()
        ]
        await cursor.close()
        await connection.ensure_closed()
        return collection

    async def get_quote_data(self, date_from: str, date_to: str) -> InstrumentQuoteCollection:
        collection = InstrumentQuoteCollection()
        connection = await self.__get_connection()
        cursor = await connection.cursor()
        await cursor.execute("""
                SELECT
                    oq.option_id,
                    AVG(oq.bid) AS bid,
                    AVG(oq.ask) AS ask,
                    SUM(oq.bid_volume) AS bid_volume,
                    SUM(oq.ask_volume) AS ask_volume,
                    o.strike,
                    o.expiration,
                    o.direction
                FROM
                    option_quote oq
                INNER JOIN
                    option_data o ON o.id = oq.option_id
                WHERE
                    oq.created_at BETWEEN %s and %s
                GROUP BY 1
            """, (date_from, date_to))

        [
            collection.add(InstrumentQuote(
                id=quote[0],
                bid=quote[1],
                ask=quote[2],
                bid_volume=quote[3],
                ask_volume=quote[4],
                tracked_at=date_from,
                strike=quote[5],
                expiration=quote[6],
                direction=quote[7],
            ))
            for quote in await cursor.fetchall()
        ]
        await cursor.close()
        await connection.ensure_closed()
        return collection

    async def __get_connection(self):
        if self.__connection is None or self.__connection.closed:
            self.__connection = await self.__mysql.create_connection()
        return self.__connection


class AssetProviderMysql(DataProviderAbstract):
    def __init__(self, mysql: ConnectionProvider):
        self.__mysql = mysql
        self.__connection = None

    async def get_data(self) -> InstrumentCollection:
        collection = InstrumentCollection()
        connection = await self.__get_connection()
        cursor = await connection.cursor()
        await cursor.execute("SELECT id, symbol FROM asset WHERE is_active = 1")
        [
            collection.add(Instrument(id=asset[0], symbol=asset[1], parent_id=None))
            for asset in await cursor.fetchall()
        ]
        await cursor.close()
        await connection.ensure_closed()
        return collection

    async def get_quote_data(self, date_from: str, date_to: str) -> InstrumentQuoteCollection:
        collection = InstrumentQuoteCollection()
        connection = await self.__get_connection()
        cursor = await connection.cursor()
        await cursor.execute("""
                SELECT
                    asset_id,
                    AVG(bid) AS bid,
                    AVG(ask) AS ask,
                    SUM(bid_volume) AS bid_volume,
                    SUM(ask_volume) AS ask_volume
                FROM
                    asset_quote
                WHERE
                    created_at BETWEEN %s and %s
                GROUP BY 1
            """, (date_from, date_to))
        [
            collection.add(InstrumentQuote(
                id=quote[0],
                bid=quote[1],
                ask=quote[2],
                bid_volume=quote[3],
                ask_volume=quote[4],
                tracked_at=date_from,
                strike=None,
                expiration=None,
                direction=None
            ))
            for quote in await cursor.fetchall()
        ]
        await cursor.close()
        await connection.ensure_closed()
        return collection

    async def __get_connection(self):
        if self.__connection is None or self.__connection.closed:
            self.__connection = await self.__mysql.create_connection()
        return self.__connection
