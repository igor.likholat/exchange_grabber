import asyncio
from logging import Logger
from service.option_grabber.data_provider import DataProviderAbstract
from service.option_grabber.queue_driver import QueueDriverAbstract
from service.worker import WorkerAbstract


class Producer(WorkerAbstract):
    def __init__(
            self,
            logger: Logger,
            queue_driver: QueueDriverAbstract,
            option_provider: DataProviderAbstract,
    ):
        self.__logger = logger
        self.__queue_driver = queue_driver
        self.__option_provider = option_provider

    async def before_start(self, **kwargs) -> None:
        pass

    async def handle_tick(self) -> None:
        await self.__queue_driver.produce(await self.__option_provider.get_data())

    async def stop(self) -> None:
        pass
