from abc import ABC
from numpy import isnan

from service.mysql.connection_provider import ConnectionProvider
from service.option_grabber.model import InstrumentQuoteCollection


class DataSaverAbstract(ABC):
    async def save_price(self, quotes: list) -> None:
        raise NotImplementedError()


class VolatilitySaverMysql:
    def __init__(self, mysql: ConnectionProvider):
        self.__mysql = mysql
        self.__connection = None

    async def save(self, volatility_list: list) -> None:
        connection = await self.__get_connection()
        try:
            cursor = await connection.cursor()
            insert_volatility = """
                INSERT INTO
                    volatility (
                        option_id,
                        asset_id,
                        tracked_at,
                        option_bid,
                        option_ask,
                        option_bid_volume,
                        option_ask_volume,
                        asset_bid,
                        asset_ask,
                        asset_bid_volume,
                        asset_ask_volume,
                        volatility
                    )
                VALUES
                    (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                """
            rows = []
            for volatility_data in volatility_list:
                rows.append((
                    volatility_data.option_id,
                    volatility_data.asset_id,
                    volatility_data.tracked_at,
                    volatility_data.option_bid,
                    volatility_data.option_ask,
                    volatility_data.option_bid_volume,
                    volatility_data.option_ask_volume,
                    volatility_data.asset_bid,
                    volatility_data.asset_ask,
                    volatility_data.asset_bid_volume,
                    volatility_data.asset_ask_volume,
                    (volatility_data.volatility if not isnan(volatility_data.volatility) else 0),
                ))
            await cursor.executemany(insert_volatility, rows)
            await connection.commit()
        except Exception as e:
            print('mysql {}'.format(str(e)))
        await connection.ensure_closed()

    async def __get_connection(self):
        if self.__connection is None or self.__connection.closed:
            self.__connection = await self.__mysql.create_connection()
        return self.__connection


class OptionDataSaverMysql(DataSaverAbstract):
    def __init__(self, mysql: ConnectionProvider):
        self.__mysql = mysql
        self.__connection = None

    async def save_price(self, quotes) -> None:
        connection = await self.__get_connection()
        try:
            cursor = await connection.cursor()
            insert_price = """
                INSERT INTO
                    option_quote (option_id, bid, bid_volume, ask, ask_volume, tracked_at)
                VALUES
                    (%s, %s, %s, %s, %s, %s)
            """

            rows = []
            for quote in quotes:
                rows.append((
                    quote['id'],
                    quote['bid'],
                    quote['bid_volume'],
                    quote['ask'],
                    quote['ask_volume'],
                    quote['tracked_at'],
                ))
            await cursor.executemany(insert_price, rows)
            await connection.commit()
        except Exception as e:
            print('mysql {}'.format(str(e)))
        await connection.ensure_closed()

    async def __get_connection(self):
        if self.__connection is None or self.__connection.closed:
            self.__connection = await self.__mysql.create_connection()
        return self.__connection


class AssetDataSaverMysql(DataSaverAbstract):
    def __init__(self, mysql: ConnectionProvider):
        self.__mysql = mysql
        self.__connection = None

    async def save_price(self, quotes: InstrumentQuoteCollection) -> None:
        connection = await self.__get_connection()
        try:
            cursor = await connection.cursor()
            insert_price = """
                INSERT INTO
                    asset_quote (asset_id, bid, bid_volume, ask, ask_volume, tracked_at)
                VALUES
                    (%s, %s, %s, %s, %s, %s)
            """

            rows = []
            for quote in quotes:
                rows.append((
                    quote['id'],
                    quote['bid'],
                    quote['bid_volume'],
                    quote['ask'],
                    quote['ask_volume'],
                    quote['tracked_at'],
                ))
            await cursor.executemany(insert_price, rows)
            await connection.commit()
        except Exception as e:
            print('mysql {}'.format(str(e)))
        await connection.ensure_closed()

    async def __get_connection(self):
        if self.__connection is None or self.__connection.closed:
            self.__connection = await self.__mysql.create_connection()
        return self.__connection
