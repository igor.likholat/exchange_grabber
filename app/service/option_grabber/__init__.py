from service.mysql.connection_provider import ConnectionProvider as MysqlConnectionProvider
from .consumer import OptionConsumerProcessorExante
from .data_provider import OptionProviderMysql, AssetProviderMysql
from .data_saver import OptionDataSaverMysql, AssetDataSaverMysql, VolatilitySaverMysql


def create_option_provider_mysql(
        mysql: MysqlConnectionProvider
) -> OptionProviderMysql:
    return OptionProviderMysql(mysql)


def create_asset_provider_mysql(
        mysql: MysqlConnectionProvider
) -> AssetProviderMysql:
    return AssetProviderMysql(mysql)


def create_option_saver_mysql(
        mysql: MysqlConnectionProvider
) -> OptionDataSaverMysql:
    return OptionDataSaverMysql(mysql)


def create_asset_saver_mysql(
        mysql: MysqlConnectionProvider
) -> AssetDataSaverMysql:
    return AssetDataSaverMysql(mysql)


def create_volatility_saver_mysql(
        mysql: MysqlConnectionProvider
) -> VolatilitySaverMysql:
    return VolatilitySaverMysql(mysql)
