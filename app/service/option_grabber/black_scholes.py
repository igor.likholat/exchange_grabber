"""
Legend of Variables
___________________
    s: stock price
    x: strike price
    mp: market price of option
    bs_p: price of an option as per Black Scholes
    r: risk-free interest rate
    sigma: standard deviation of log returns (volatility)
    fs: future stock price
    ts: today's stock price
    u: periodic daily rate of return
    tau: time to expiration in years
    vega: derivative of option price with respect to volatility
    N(d1):
        For call option: future value of stock given stock_price >= strike_price at option expiration
        For put option: future value of stock given stock_price < strike_price at option expiration
    N(d2):
        For call option: P(stock_price >= strike_price) at option expiration
        For put option: P(stock_price < strike_price) at option expiration
"""
import math
import operator
import numpy as np
import scipy.stats as stats
from datetime import datetime


class BlackScholes:
    def __init__(self):
        pass

    @staticmethod
    def normal_pdf(y, mean=0, sigma=1.0):
        numerator = math.exp((-1 * math.pow((y - mean), 2)) / 2 * math.pow(sigma, 2))
        denominator = math.sqrt(2.0 * math.pi) * sigma
        return numerator / denominator

    @staticmethod
    def normal_cdf(z, mean=0, sigma=1.0):
        return stats.norm.cdf(z, mean, sigma)

    @staticmethod
    def d(s, x, r, sigma, tau, func_operator=operator.add):
        numerator = math.log(s / x) + func_operator(r, math.pow(sigma, 2) / 2) * tau
        denominator = sigma * math.sqrt(tau)
        return numerator / denominator

    @staticmethod
    def mean(r, sigma):
        return r - (math.pow(sigma, 2) / 2)

    @staticmethod
    def time_to_expiry(expiry_date):
        """ Calculate time to expiry of option expressed as fraction of year
        Args:
            expiry_date (datetime): expiration date
        Returns:
            float: fraction of year
        """

        current = datetime.now()
        seconds_in_year = 31536000
        return (expiry_date - current).total_seconds() / seconds_in_year

    @staticmethod
    def option_payoff(s, x, mp, option_type="call"):
        """ Calculate the potential profit if exercised (
                For call option: strike price < stock price, max(s - x, 0) - op
                For put option: strike price > stock price, max(x - s, 0) - op
            )
        Args:
            s (float): stock price
            x (float): strike price
            mp (float): market price of an option
            option_type (str): specify option type: "call" or "put"
        Returns:
            float: potential profit
        """
        if option_type == "put":
            return np.maximum(s - x, 0) - mp
        else:
            return np.maximum(x - s, 0) - mp

    def option_price(self, s, x, r, sigma, tau, option_type="call"):
        """ Calculate option price as per Black Scholes (
                For call option: s * N(d1)] - [x * e^(-r * tau) * N(d2)]
                For put option: x * e^(-r * tau) * N(-d2)] - [s * N(-d1)
            )

        Args:
            s (float): stock price
            x (float): strike price
            r (float): risk-free interest rate
            sigma (float): standard deviation of log returns (volatility)
            tau (float): time to option expiration expressed in years
            option_type (str): specify option type: "call" or "put"
        Returns:
            float: option price
        """
        d1 = self.d(s, x, r, sigma, tau, operator.add)
        d2 = self.d(s, x, r, sigma, tau, operator.sub)

        if option_type == "put":
            d1 = -1 * d1
            d2 = -1 * d2

        weighted_stock_price = s * self.normal_cdf(d1)

        # Discount strike price at expiration to present value
        discounted_strike_price = x * math.exp(-r * tau)
        # Multiply by probability of option having value at expiration
        weighted_strike_price = discounted_strike_price * self.normal_cdf(d2)

        if option_type == "put":
            return weighted_strike_price - weighted_stock_price
        else:
            return weighted_stock_price - weighted_strike_price

    def _vega(self, s, x, r, sigma, tau):
        """ Calculate derivative of option price with respect to volatility
            vega = s * tau^(1/2) * N(d1)
        Args:
            s (float): stock price
            x (float): strike price
            r (float): risk-free interest rate
            sigma (float: standard deviation of log returns (volatility)
            tau (float): time to option expiration expressed in years
        Returns:
            float: vega
        """

        d1 = self.d(s, x, r, sigma, tau, operator.add)

        return s * math.sqrt(tau) * self.normal_pdf(d1)

    def implied_volatility(self, s, x, r, tau, mp, option_type="call", precision=1e-4, iterations=100):
        """ Newton-Raphson method of successive approximations to find implied volatility
        Args:
            s (float): stock price
            x (float): strike price
            r (float): risk-free interest rate
            tau (float): time to option expiration expressed in years
            mp (float): market price of an option
            option_type (str): specify option type: "call" or "put"
            precision (float): threshold below which to accept volatility estimate
            iterations (int): number of rounds of estimations to conduct
        Returns:
            float: closest estimation for implied volatility
        """

        # initial estimation
        sigma = 0.5
        for i in range(0, iterations):
            # price of an option as per Black Scholes
            bs_p = self.option_price(s, x, r, sigma, tau, option_type)
            diff = mp - bs_p
            # check if difference is acceptable
            if operator.abs(diff) < precision:
                return sigma

            vega = self._vega(s, x, r, sigma, tau)
            if vega == 0:
                return 0
            # update sigma with addition of difference divided by derivative
            try:
                sigma = sigma + (diff / vega)
            except RuntimeWarning as e:
                sigma = 0

        # closest estimation
        return sigma
