import asyncio
import inject
from di.services import services


if __name__ == "__main__":
    inject.configure(services)
    consumer = inject.instance('price_consumer_asset')
    loop = asyncio.get_event_loop()
    connection = loop.run_until_complete(consumer.run(loop))

    try:
        loop.run_forever()
    finally:
        loop.run_until_complete(connection.close())
