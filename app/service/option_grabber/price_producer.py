import asyncio
import aio_pika
import json
from logging import Logger
from service.rabbitmq.connection_provider import ConnectionProvider


class PriceProducer:
    def __init__(
            self,
            rabbitmq: ConnectionProvider,
            queue_name: str,
            logger: Logger
    ):
        self.__rabbitmq = rabbitmq
        self.__queue_name = queue_name
        self.__logger = logger
        self.__connection = None
        self.__channel = None

    async def produce(self, quotes: list) -> None:
        connection = await self.__get_connection()
        channel = await connection.channel()

        await channel.default_exchange.publish(
            aio_pika.Message(body=json.dumps(quotes).encode()),
            routing_key=self.__queue_name,
        )
        await connection.close()

    async def __get_connection(self):
        if self.__connection is None or self.__connection.is_closed:
            self.__connection = await self.__rabbitmq.create_connection()
        return self.__connection

    async def __get_channel(self, connection):
        if self.__channel is None or self.__channel.is_closed:
            self.__channel = await connection.channel()
            await self.__channel.set_qos(prefetch_count=100)
        return self.__channel
