import asyncio
import inject
from di.services import services


if __name__ == "__main__":
    inject.configure(services)
    producer = inject.instance('option_quote_stream_exante_producer')
    asyncio.run(producer.read())
