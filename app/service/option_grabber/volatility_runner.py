import asyncio
import inject
import datetime
from async_cron.job import CronJob
from async_cron.schedule import Scheduler
from di.services import services


if __name__ == "__main__":
    inject.configure(services)
    volatility = inject.instance('volatility')

    run_period_min = 3

    # Uncomment for local debug
    # now = datetime.datetime.now()
    # now.replace(tzinfo=datetime.timezone.utc)
    #
    # date_to = datetime.datetime(now.year, now.month, now.day, now.hour, now.minute, 0, 0)
    # delta_period = datetime.timedelta(minutes=3)
    # date_from = date_to - delta_period
    #
    # loop = asyncio.get_event_loop()
    # loop.run_until_complete(volatility.calculate(date_from, date_to))
    # exit()

    async def volatility_runner():
        now = datetime.datetime.now()
        now.replace(tzinfo=datetime.timezone.utc)

        delta_correction = datetime.timedelta(minutes=run_period_min)
        date_to = datetime.datetime(now.year, now.month, now.day, now.hour, now.minute, 0, 0) - delta_correction

        delta_period = datetime.timedelta(minutes=run_period_min)
        date_from = date_to - delta_period

        await volatility.calculate(date_from, date_to)

    msh = Scheduler(locale="en_US")
    myjob = CronJob(name='volatility_calculation').every(60 * run_period_min).second.go(volatility_runner)
    msh.add_job(myjob)
    loop = asyncio.get_event_loop()

    try:
        loop.run_until_complete(msh.start())
    except KeyboardInterrupt:
        print('exit')
