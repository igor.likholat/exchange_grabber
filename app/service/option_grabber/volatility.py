import asyncio
from datetime import datetime

from service.option_grabber import VolatilitySaverMysql
from service.option_grabber.black_scholes import BlackScholes
from service.option_grabber.data_provider import DataProviderAbstract
from service.option_grabber.model import VolatilityData


class Volatility:
    ROUND_ACCURACY = 6
    RECORDS_PER_INSERT = 500

    def __init__(
            self,
            option_data_provider: DataProviderAbstract,
            asset_data_provider: DataProviderAbstract,
            volatility_data_saver: VolatilitySaverMysql,
            volatility_method: BlackScholes,
            logger,
    ):
        self.__option_data_provider = option_data_provider
        self.__asset_data_provider = asset_data_provider
        self.__volatility_data_saver = volatility_data_saver
        self.__volatility_method = volatility_method
        self.__logger = logger
        self.__option_asset_map = None
        self.__calculated_stack = []

    async def calculate(self, date_from: str, date_to: str) -> None:
        self.__reset_calculated_stack()
        data_collection = await self.__prepare_data(date_from, date_to)
        tasks = [
            self.__make_calculation(data)
            for data in data_collection
        ]
        await asyncio.gather(*tasks)
        await self.__try_to_save_calculated_stack(hard_save=True)

    async def __make_calculation(self, data: VolatilityData):
        try:
            data.volatility = self.__round(float(self.__volatility_method.implied_volatility(
                s=float(data.get_asset_price()),
                x=float(data.strike),
                r=0.05,
                tau=self.__volatility_method.time_to_expiry(data.expiration),
                mp=float(data.get_option_price()),
                option_type=data.direction,
            )))
        except Exception as e:
            # todo write some log
            data.volatility = 0
        self.__calculated_stack.append(data)
        await self.__try_to_save_calculated_stack()

    async def __prepare_data(self, date_from: str, date_to: str) -> list:
        await self.__init_option_asset_map()
        option_quotes = await self.__option_data_provider.get_quote_data(date_from, date_to)
        asset_quotes = await self.__asset_data_provider.get_quote_data(date_from, date_to)

        asset_quote_map = {}
        for asset_quote in asset_quotes:
            asset_quote_map[str(asset_quote.id)] = asset_quote

        prepared_collection = []
        for option_quote in option_quotes:
            idx = self.__option_asset_map.get(str(option_quote.id))
            if idx is not None and asset_quote_map[idx] is not None:
                asset_quote = asset_quote_map[self.__option_asset_map[str(option_quote.id)]]
                prepared_collection.append(VolatilityData(
                    option_id=option_quote.id,
                    asset_id=asset_quote.id,
                    tracked_at=date_from,
                    option_bid=self.__round(option_quote.bid),
                    option_ask=self.__round(option_quote.ask),
                    option_bid_volume=self.__round(option_quote.bid_volume),
                    option_ask_volume=self.__round(option_quote.ask_volume),
                    asset_bid=self.__round(asset_quote.bid),
                    asset_ask=self.__round(asset_quote.ask),
                    asset_bid_volume=self.__round(asset_quote.bid_volume),
                    asset_ask_volume=self.__round(asset_quote.ask_volume),
                    strike=option_quote.strike,
                    expiration_days=await self.__get_expiration_days(option_quote.expiration),
                    expiration=option_quote.expiration,
                    direction=option_quote.direction,
                    volatility=0,
                ))
        return prepared_collection

    def __reset_calculated_stack(self) -> None:
        self.__calculated_stack = []

    async def __try_to_save_calculated_stack(self, hard_save: bool = False) -> None:
        stack_len = len(self.__calculated_stack)
        if (stack_len >= self.RECORDS_PER_INSERT) or (hard_save and stack_len > 0):
            data_buffer = self.__calculated_stack
            self.__reset_calculated_stack()
            await self.__volatility_data_saver.save(data_buffer)

    def __round(self, value: float) -> float:
        return round(value, self.ROUND_ACCURACY)

    async def __init_option_asset_map(self) -> None:
        if self.__option_asset_map is None:
            self.__option_asset_map = {}
            options = await self.__option_data_provider.get_data()
            for option in options:
                self.__option_asset_map[str(option.id)] = str(option.parent_id)

    @staticmethod
    async def __get_expiration_days(expiration: datetime) -> int:
        now = datetime.now()
        days = (expiration - now).days
        return days if days >= 0 else 0
