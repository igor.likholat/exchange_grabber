from dataclasses import dataclass
from datetime import datetime


@dataclass(frozen=True)
class Instrument:
    id: int
    symbol: str
    parent_id: int or None


@dataclass(frozen=True)
class InstrumentQuote:
    id: int
    tracked_at: str
    bid: float
    ask: float
    bid_volume: float
    ask_volume: float
    strike: float or None
    expiration: datetime or None
    direction: str or None


@dataclass(frozen=False)
class VolatilityData:
    option_id: int
    asset_id: int
    tracked_at: str
    option_bid: float
    option_ask: float
    option_bid_volume: float
    option_ask_volume: float
    asset_bid: float
    asset_ask: float
    asset_bid_volume: float
    asset_ask_volume: float
    expiration_days: int
    expiration: datetime
    strike: float
    direction: str
    volatility: float

    def get_option_price(self) -> float:
        return (self.option_ask + self.option_bid) / 2

    def get_asset_price(self) -> float:
        return (self.asset_ask + self.asset_bid) / 2


class InstrumentCollection:
    def __init__(self, instruments: [Instrument] = ()):
        self.__items = []
        [self.add(instrument) for instrument in instruments]

    def add(self, result: Instrument) -> None:
        self.__items.append(result)

    def __iter__(self) -> [Instrument]:
        return iter(self.__items)


class InstrumentQuoteCollection:
    def __init__(self, quotes: [InstrumentQuote] = ()):
        self.__items = []
        [self.add(quote) for quote in quotes]

    def add(self, result: InstrumentQuote) -> None:
        self.__items.append(result)

    def len(self) -> int:
        return len(self.__items)

    def __iter__(self) -> [InstrumentQuote]:
        return iter(self.__items)
