import itertools

from service.broker.exante.client import ClientAbstract
from service.option_grabber.data_provider import DataProviderAbstract


class QuoteReader:
    def __init__(
            self,
            broker_client: ClientAbstract,
            data_provider: DataProviderAbstract,
            instruments_per_request: int,
    ):
        self.__broker_client = broker_client
        self.__data_provider = data_provider
        self.__instruments_per_request = instruments_per_request

    async def read_data(self):
        instruments = await self.__data_provider.get_data()
        await self.__broker_client.get_prices(
            instruments=instruments,
            chunk_size=self.__instruments_per_request,
        )
