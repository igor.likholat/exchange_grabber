import asyncio
import itertools
from abc import ABC
from logging import Logger
from service.broker.exante.client import ClientAbstract
from service.option_grabber.data_provider import DataProviderAbstract


class ProducerStreamAbstract(ABC):
    async def read(self) -> None:
        raise NotImplementedError()


class ProducerStream:
    def __init__(
            self,
            client: ClientAbstract,
            data_provider: DataProviderAbstract,
            logger: Logger,
            items_in_stream: int,
    ):
        self.__client = client
        self.__logger = logger
        self.__data_provider = data_provider
        self.__items_in_stream = items_in_stream

    async def read(self) -> None:
        instruments = await self.__data_provider.get_data()
        tasks = [
            self.__client.quote_stream(collection)
            for collection in self.__chunk_collection(self.__items_in_stream, instruments)
        ]
        await asyncio.gather(*tasks)

    @staticmethod
    def __chunk_collection(n, iterable):
        it = iter(iterable)
        while True:
            chunk = tuple(itertools.islice(it, n))
            if not chunk:
                return
            yield chunk
