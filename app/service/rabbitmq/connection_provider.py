from dataclasses import dataclass
import asyncio
import aio_pika


@dataclass(frozen=True)
class Config:
    host: str
    login: str
    password: str
    vhost: str
    port: int


class ConnectionProvider:
    def __init__(self, config: Config):
        self.__config = config

    async def create_connection(self, loop=None):
        return await aio_pika.connect(
            host=self.__config.host,
            login=self.__config.login,
            password=self.__config.password,
            virtualhost=self.__config.vhost,
            port=self.__config.port,
            loop=loop,
        )
