from service.sender.model import TelegaConfig
from service.sender.telega import Telega


def create_sender_telega(config: TelegaConfig) -> Telega:
    return Telega(config=config)
