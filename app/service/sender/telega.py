import logging
import telegram

from service.sender import TelegaConfig


class Telega:
    def __init__(self, config: TelegaConfig):
        self.__token = config.token
        self.__channel = config.channel
        self.__bot = None
        self.__init_bot()
        self.__init_logger()

    def send(self, message: str) -> None:
        self.__bot.sendMessage(chat_id=self.__channel, text=message, timeout=15)

    def __init_bot(self) -> None:
        if self.__bot is None:
            self.__bot = telegram.Bot(token=self.__token)

    @staticmethod
    def __init_logger() -> None:
        logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                            level=logging.INFO)
