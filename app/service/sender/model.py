from dataclasses import dataclass


@dataclass(frozen=True)
class TelegaConfig:
    token: str
    channel: str
