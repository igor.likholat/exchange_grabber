from dataclasses import dataclass


@dataclass(frozen=True)
class Config:
    endpoint: str
    client_id: str
    app_id: str
    shared_key: str
    env: str
    token_ttl_sec: int


@dataclass(frozen=True)
class OptionRequest:
    id: int
    symbol: str


@dataclass(frozen=True)
class QuoteResponse:
    id: int
    symbol: str
    tracked_at: str
    bid: float
    ask: float
    bid_volume: float
    ask_volume: float


class OptionRequestCollection:
    def __init__(self, options: [OptionRequest] = ()):
        self.__items = []
        [self.add(option) for option in options]

    def add(self, result: OptionRequest) -> None:
        self.__items.append(result)

    def __iter__(self) -> [OptionRequest]:
        return iter(self.__items)
