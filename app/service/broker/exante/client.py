import asyncio
import itertools
import json

import aiohttp
import jwt
import time
from abc import ABC
from datetime import datetime

from service.broker.exante.model import OptionRequest, Config, QuoteResponse
from service.option_grabber.model import InstrumentCollection
from service.option_grabber.price_producer import PriceProducer


class ClientAbstract(ABC):
    async def get_price(self, request: OptionRequest) -> QuoteResponse or None:
        raise NotImplementedError()

    async def get_prices(self, instruments: InstrumentCollection, chunk_size: int = 1):
        raise NotImplementedError()

    async def quote_stream(self, request: tuple) -> None:
        raise NotImplementedError()


class Client(ClientAbstract):
    MAX_QUOTES = 100
    QUOTES_TTL = 10

    ACTION_LAST_QUOTE = '{api_endpoint}feed/{symbol_id}/last'
    ACTION_QUOTE_STREAM = '{api_endpoint}feed/{symbols}'

    def __init__(self, config: Config, price_producer: PriceProducer):
        self.__config = config
        self.__price_producer = price_producer
        self.__token_init_time = 0
        self.__instrument_map = None
        self.__prices_result = None
        self.__token = jwt.encode({
                'sub': self.__config.app_id,
                'iss': self.__config.client_id,
                'iat': self.__token_init_time,
                'aud': [
                    'feed',
                    'symbols',
                    'ohlc',
                    'crossrates'
                ]
            }, self.__config.shared_key, algorithm='HS256')

    async def get_prices(self, instruments: InstrumentCollection, chunk_size: int = 1):
        self.__prices_result = None
        self.__build_instrument_map(instruments)

        tasks = []
        sem = asyncio.Semaphore(150)

        async with aiohttp.ClientSession(headers=self.__get_headers()) as session:
            for chunk in self.__chunk_collection(chunk_size, instruments):
                request_string = ','.join([item.symbol for item in chunk])
                url = self.__get_url(self.ACTION_LAST_QUOTE, {'symbol_id': request_string})

                task = asyncio.ensure_future(self.__bound_fetch(sem, url, session))
                tasks.append(task)

            await asyncio.gather(*tasks)

        if self.__prices_result is not None and len(self.__prices_result) > 0:
            await self.__price_producer.produce(quotes=self.__prices_result)
            self.__prices_result = None

    async def __bound_fetch(self, sem, url, session):
        async with sem:
            await self.__fetch(url, session)

    async def __fetch(self, url, session):
        if self.__prices_result is None:
            self.__prices_result = []

        async with session.get(url) as response:
            if response.status == 200:
                try:
                    quote_items = await response.json()
                    for data in quote_items:
                        self.__prices_result.append({
                            'id': self.__instrument_map[data['symbolId']],
                            'tracked_at': datetime.utcfromtimestamp(data['timestamp'] // 1000).strftime(
                                '%Y-%m-%d %H:%M:%S'),
                            'bid': float(data['bid'][0]['value'] if len(data['bid']) else 0),
                            'bid_volume': float(data['bid'][0]['size'] if len(data['bid']) else 0),
                            'ask': float(data['ask'][0]['value'] if len(data['ask']) else 0),
                            'ask_volume': float(data['ask'][0]['size'] if len(data['ask']) else 0),
                        })
                        if len(self.__prices_result) == self.MAX_QUOTES:
                            await self.__price_producer.produce(quotes=self.__prices_result)
                            self.__prices_result = []
                except IndexError as e:
                    # todo write log
                    print('{} {}'.format(url, str(e)))
                    pass
            else:
                # todo write log
                print('{} {}'.format(url, await response.content.readline()))
                pass

    def __build_instrument_map(self, instruments: InstrumentCollection) -> None:
        if self.__instrument_map is None:
            self.__instrument_map = {}
            [self.__instrument_map.update({item.symbol: item.id}) for item in instruments]

    async def get_price(self, request: OptionRequest) -> QuoteResponse or None:
        async with aiohttp.ClientSession(headers=self.__get_headers()) as session:
            url = self.__get_url(self.ACTION_LAST_QUOTE, {'symbol_id': request.symbol})
            response = await session.get(url)
            if response.status == 200:
                try:
                    data = await response.json()
                    data = data[0]
                    result = QuoteResponse(
                        id=request.id,
                        symbol=data['symbolId'],
                        tracked_at=datetime.utcfromtimestamp(data['timestamp'] // 1000).strftime('%Y-%m-%d %H:%M:%S'),
                        bid=float(data['bid'][0]['value']),
                        bid_volume=float(data['bid'][0]['size']),
                        ask=float(data['ask'][0]['value']),
                        ask_volume=float(data['ask'][0]['size']),
                    )
                except IndexError as e:
                    # todo write log
                    print('{} {}'.format(request.symbol, str(e)))
                    result = None
            else:
                # todo write log
                print('{} {} {}'.format(request.symbol, response.status, await response.content.readline()))
                result = None
        return result

    async def quote_stream(self, request: tuple) -> None:
        request_string = ','.join([item.symbol for item in request])
        timeout = aiohttp.ClientTimeout(total=0)
        async with aiohttp.ClientSession(
                headers=self.__get_headers({'Accept': 'application/x-json-stream'}),
                timeout=timeout) as session:
            url = self.__get_url(self.ACTION_QUOTE_STREAM, {'symbols': request_string})

            try:
                async with session.get(url) as resp:
                    quotes = None
                    quote_init_time = None
                    while True:
                        if quotes is None:
                            quotes = []
                        if quote_init_time is None:
                            quote_init_time = int(time.time())

                        chunk = await resp.content.readline()
                        if not chunk:
                            break
                        json_data = chunk.decode('utf8').strip()
                        try:
                            data = json.loads(json_data)
                        except json.decoder.JSONDecodeError:
                            continue
                        if self.__is_valid_data(data):
                            instrument = list(
                                filter(
                                    lambda instr: instr.symbol == data.get('symbolId'), request
                                )
                            )
                            if len(instrument):
                                quotes.append({
                                    'id': instrument[0].id,
                                    'tracked_at': datetime.utcfromtimestamp(data['timestamp'] // 1000).strftime(
                                        '%Y-%m-%d %H:%M:%S'),
                                    'bid': float(data['bid'][0]['value'] if len(data['bid']) else 0),
                                    'bid_volume': float(data['bid'][0]['size'] if len(data['bid']) else 0),
                                    'ask': float(data['ask'][0]['value'] if len(data['ask']) else 0),
                                    'ask_volume': float(data['ask'][0]['size'] if len(data['ask']) else 0),
                                })
                        if len(quotes) == self.MAX_QUOTES or\
                                (len(quotes) > 0 and int(time.time()) >= quote_init_time + self.QUOTES_TTL):
                            await self.__price_producer.produce(quotes=quotes)
                            quotes = None
                            quote_init_time = None
                        # todo think about this line
                        await asyncio.sleep(0.2)
            except Exception as ex:
                pass
            finally:
                pass

    @staticmethod
    def __is_valid_data(data: dict) -> bool:
        return data.get('symbolId') is not None and data.get('bid') is not None and data.get('ask') is not None

    def __get_token(self) -> str:
        if self.__token is None or self.__is_token_expire():
            self.__token_init_time = int(time.time())
            self.__token = jwt.encode({
                'sub': self.__config.app_id,
                'iss': self.__config.client_id,
                'iat': self.__token_init_time,
                'aud': [
                    'feed',
                    'symbols',
                    'ohlc',
                    'crossrates'
                ]
            }, self.__config.shared_key, algorithm='HS256').decode('utf-8')
        return self.__token

    def __get_headers(self, headers: dict = None) -> dict:
        result = {
            'Authorization': 'Bearer {}'.format(self.__token)
        }
        if headers:
            result = {**result, **headers}
        return result

    def __get_url(self, template: str, data: dict = None) -> str:
        return template.format(api_endpoint=self.__config.endpoint, **(data or {}))

    def __is_token_expire(self) -> bool:
        return int(time.time()) >= self.__token_init_time + self.__config.token_ttl_sec

    @staticmethod
    def __chunk_collection(n, iterable):
        it = iter(iterable)
        while True:
            chunk = tuple(itertools.islice(it, n))
            if not chunk:
                return
            yield chunk
