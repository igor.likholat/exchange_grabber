from service.broker.exante.client import Client as ExanteClient
from service.broker.exante.model import Config
from service.option_grabber.price_producer import PriceProducer


def get_client(config: Config, price_producer: PriceProducer) -> ExanteClient:
    return ExanteClient(config, price_producer)
