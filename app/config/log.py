from config.config import get_config
import logging.config

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'handlers': {
        'console': {
            'level': logging.DEBUG,
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console'],
            'level': logging.WARNING,
            'propagate': False,
        },
        'option_grabber': {
            'handlers': ['console'],
            'level': get_config().get('OptionGrabber').get('log_level', logging.WARNING),
            'propagate': False,
        },
    }
}


def config_logging() -> None:
    logging.config.dictConfig(LOGGING)
