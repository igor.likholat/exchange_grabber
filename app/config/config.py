import os


CONFIG = {
    'OptionGrabber': {
        'log_level': int(os.getenv('OptionGrabber_log_level', 0)) or None,
        'num_worker_threads': 1
    },
    'Mysql': {
        'host': os.getenv('Mysql_host', '127.0.0.1'),
        'port': os.getenv('Mysql_port', 3306),
        'user': os.getenv('Mysql_user', 'grabber'),
        'password': os.getenv('Mysql_password', 'password'),
        'db': os.getenv('Mysql_db', 'exchange_grabber'),
    },
    'Rabbitmq': {
        'host': os.getenv('Rabbit_host', 'exchange_grabber_rabbitmq'),
        'login': os.getenv('Rabbit_login', 'user'),
        'password': os.getenv('Rabbit_password', 'pass'),
        'vhost': os.getenv('Rabbit_vhost', 'vhost'),
        'port': os.getenv('Rabbit_port', 5672),
        'queue_option_quote': 'option_quote',
        'threads_per_queue': 15,
    },
    'Exante': {
        'endpoint': 'https://api-demo.exante.eu/md/2.0/',
        'client_id': os.getenv('Exante_client_id', '2d0c8586-9851-4440-9df8-4f5a3f26c4db'),
        'app_id': os.getenv('Exante_app_id', 'e77db611-2045-4690-a8d5-f91b1aa3bb8f'),
        'shared_key': os.getenv('Exante_shared_key', '8LNVzWmH11VGkBP7F8qyi4Qwgh4woOMx'),
        'env': os.getenv('Exante_shared_key', 'demo'),
        'token_ttl_sec': 300
    },
    'TickerProviderFile': {
        'file_path': 'keywords/tickers.txt',
    },
    'Telega': {
        'token': os.getenv('Telega_token', '1899189450:AAFJaXeIjo5yoaJIUXwPuur2MhoJYN6Ud4s'),
        'channel': '@NewsFeederTrendsAlert',
    },
}


def get_config() -> dict:
    return CONFIG
