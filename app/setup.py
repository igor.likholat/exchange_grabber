from setuptools import setup, find_packages
from setuptools.command.develop import develop
from setuptools.command.install import install

ERROR_CODE = 1
name = 'exchange_data'
version = '1.0'
config_dist_path = 'config/config.py'


def post_setup():
    pass


class PostInstall(install):
    def run(self):
        install.run(self)
        post_setup()


class PostDevelop(develop):
    def run(self):
        develop.run(self)
        post_setup()


config = {
    'name': name,
    'version': version,
    'packages': find_packages(),
    'install_requires': [],
    'entry_points': {
        'console_scripts': [
            'worker = command.worker:start'
        ],
    },
    'cmdclass': {
        'develop': PostDevelop,
        'install': PostInstall,
    },
    'package_data': {
        'config': [config_dist_path],
    },
}

try:
    setup(**config)
except RuntimeWarning as e:
    print('setup error: {}'.format(e))
    quit(ERROR_CODE)
