import inject
from di.services import services


class App(object):
    __instance = None
    __is_built = False

    def __new__(cls):
        if App.__instance is None:
            App.__instance = object.__new__(cls)
        return App.__instance

    @staticmethod
    def get_instance(build: bool = False) -> "App":
        result = App().__instance
        if build:
            result.build()
        return result

    def build(self) -> None:
        if self.__is_built is False:
            self.__is_built = True
            inject.configure(services)
            self._init_env()

    def _init_env(self) -> None:
        pass

    def get_service(self, class_name):
        if self.__is_built is False:
            raise RuntimeError("call build first")
        return inject.instance(class_name)
