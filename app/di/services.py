import inject
import logging

from config.config import get_config
from service.broker.exante import get_client
from service.broker.exante.model import Config as ExanteConfig
from service.config.loader import ConfigLoaderDict
from service.config.service import ConfigService
from service.mysql.connection_provider import ConnectionProvider as MysqlConnectionProvider
from service.news_feeder.trends import Trends
from service.option_grabber.black_scholes import BlackScholes
from service.option_grabber.price_consumer import PriceConsumer
from service.option_grabber.price_producer import PriceProducer
from service.option_grabber.producer_stream import ProducerStream
from service.option_grabber.quote_reader import QuoteReader
from service.option_grabber.volatility import Volatility
from service.news_feeder import create_news_feeder_ticker_provider_file
from service.rabbitmq.connection_provider import ConnectionProvider as RabbitmqConnectionProvider
from service.option_grabber import create_option_provider_mysql, create_option_saver_mysql, \
    create_asset_provider_mysql, create_asset_saver_mysql, create_volatility_saver_mysql
from service.option_grabber.consumer import OptionConsumerProcessorExante, Consumer
from service.option_grabber.producer import Producer
from service.option_grabber.queue_driver import QueueDriverExante
from service.sender import create_sender_telega
from service.worker import Worker


def services(binder) -> None:
    service_config(binder)
    mysql_connection_provider(binder)
    rabbitmq_connection_provider(binder)
    option_grabber_exante(binder)
    news_feeder_ticker_provider(binder)


def service_config(binder) -> None:
    binder.bind_to_constructor(ConfigService, lambda: (
        ConfigService(
            config_loader=ConfigLoaderDict(
                get_config()
            )
        )
    ))


def news_feeder_ticker_provider(binder) -> None:
    binder.bind_to_constructor('trends_ticker', lambda: (
        Trends(
            data_provider=create_news_feeder_ticker_provider_file(
                config=inject.instance(ConfigService).get_config_file_ticker_provider()
            ),
            sender=create_sender_telega(
                config=inject.instance(ConfigService).get_config_telegram()
            ),
        )
    ))


def option_grabber_exante(binder) -> None:
    binder.bind_to_constructor('volatility', lambda: (
        Volatility(
            option_data_provider=inject.instance('option_data_provider'),
            asset_data_provider=inject.instance('data_provider_asset'),
            volatility_data_saver=inject.instance('volatility_data_saver'),
            logger=inject.instance('logger'),
            volatility_method=BlackScholes()
        )
    ))

    binder.bind_to_constructor('option_quote_reader', lambda: (
        QuoteReader(
            broker_client=inject.instance('option_broker_client_exante'),
            data_provider=inject.instance('option_data_provider'),
            instruments_per_request=1,
        )
    ))

    binder.bind_to_constructor('asset_quote_reader', lambda: (
        QuoteReader(
            broker_client=inject.instance('asset_broker_client_exante'),
            data_provider=inject.instance('data_provider_asset'),
            instruments_per_request=1,
        )
    ))

    binder.bind_to_constructor('option_quote_stream_exante_producer', lambda: (
        ProducerStream(
            client=inject.instance('option_broker_client_exante'),
            data_provider=inject.instance('option_data_provider'),
            logger=inject.instance('logger'),
            items_in_stream=5,
        )
    ))

    binder.bind_to_constructor('asset_quote_stream_exante_producer', lambda: (
        ProducerStream(
            client=inject.instance('asset_broker_client_exante'),
            data_provider=inject.instance('data_provider_asset'),
            logger=inject.instance('logger'),
            items_in_stream=5,
        )
    ))

    binder.bind_to_constructor('price_producer_asset', lambda: (
        PriceProducer(
            rabbitmq=inject.instance(RabbitmqConnectionProvider),
            queue_name='price_asset_queue',
            logger=inject.instance('logger'),
        )
    ))

    binder.bind_to_constructor('price_producer_option', lambda: (
        PriceProducer(
            rabbitmq=inject.instance(RabbitmqConnectionProvider),
            queue_name='price_option_queue',
            logger=inject.instance('logger'),
        )
    ))

    binder.bind_to_constructor('price_consumer_option', lambda: (
        PriceConsumer(
            rabbitmq=inject.instance(RabbitmqConnectionProvider),
            data_saver=inject.instance('option_data_saver'),
            queue_name='price_option_queue',
            thread_num=1,
            logger=inject.instance('logger'),
        )
    ))

    binder.bind_to_constructor('price_consumer_asset', lambda: (
        PriceConsumer(
            rabbitmq=inject.instance(RabbitmqConnectionProvider),
            data_saver=inject.instance('asset_data_saver'),
            queue_name='price_asset_queue',
            thread_num=1,
            logger=inject.instance('logger'),
        )
    ))

    binder.bind_to_constructor('option_producer_exante', lambda: (
        Worker(
            service=Producer(
                logger=inject.instance('logger'),
                queue_driver=inject.instance('queue_driver_exante'),
                option_provider=inject.instance('option_data_provider'),
            ),
            min_tick_time_sec=None
        )
    ))

    binder.bind_to_constructor('option_consumer_exante', lambda: (
        Consumer(
            rabbitmq=inject.instance(RabbitmqConnectionProvider),
            option_consumer=inject.instance('consumer_processor_exante'),
            queue_name=get_config().get('Rabbitmq').get('queue_option_quote'),
            thread_num=get_config().get('Rabbitmq').get('threads_per_queue'),
            logger=inject.instance('logger'),
        )
    ))

    binder.bind_to_constructor('logger', lambda: (
        logging.getLogger('option_grabber')
    ))

    binder.bind_to_constructor('queue_driver_exante', lambda: (
        QueueDriverExante(
            rabbitmq=inject.instance(RabbitmqConnectionProvider),
            queue_name=get_config().get('Rabbitmq').get('queue_option_quote'),
            logger=inject.instance('logger'),
        )
    ))

    binder.bind_to_constructor('option_data_provider', lambda: (
        create_option_provider_mysql(
            mysql=inject.instance(MysqlConnectionProvider)
        )
    ))

    binder.bind_to_constructor('data_provider_asset', lambda: (
        create_asset_provider_mysql(
            mysql=inject.instance(MysqlConnectionProvider)
        )
    ))

    binder.bind_to_constructor('option_data_saver', lambda: (
        create_option_saver_mysql(
            mysql=inject.instance(MysqlConnectionProvider)
        )
    ))

    binder.bind_to_constructor('asset_data_saver', lambda: (
        create_asset_saver_mysql(
            mysql=inject.instance(MysqlConnectionProvider)
        )
    ))

    binder.bind_to_constructor('volatility_data_saver', lambda: (
        create_volatility_saver_mysql(
            mysql=inject.instance(MysqlConnectionProvider)
        )
    ))

    binder.bind_to_constructor('consumer_processor_exante', lambda: (
        OptionConsumerProcessorExante(
            data_saver=inject.instance('option_data_saver'),
            client=inject.instance('option_broker_client_exante'),
            logger=inject.instance('logger'),
        )
    ))

    binder.bind_to_constructor('option_broker_client_exante', lambda: (
        get_client(
            config=ExanteConfig(
                endpoint=get_config().get('Exante').get('endpoint'),
                client_id=get_config().get('Exante').get('client_id'),
                app_id=get_config().get('Exante').get('app_id'),
                shared_key=get_config().get('Exante').get('shared_key'),
                env=get_config().get('Exante').get('env'),
                token_ttl_sec=get_config().get('Exante').get('token_ttl_sec'),
            ),
            price_producer=inject.instance('price_producer_option'),
        )
    ))

    binder.bind_to_constructor('asset_broker_client_exante', lambda: (
        get_client(
            config=ExanteConfig(
                endpoint=get_config().get('Exante').get('endpoint'),
                client_id=get_config().get('Exante').get('client_id'),
                app_id=get_config().get('Exante').get('app_id'),
                shared_key=get_config().get('Exante').get('shared_key'),
                env=get_config().get('Exante').get('env'),
                token_ttl_sec=get_config().get('Exante').get('token_ttl_sec'),
            ),
            price_producer=inject.instance('price_producer_asset'),
        )
    ))


def mysql_connection_provider(binder) -> None:
    binder.bind_to_constructor(MysqlConnectionProvider, lambda: (
        MysqlConnectionProvider(inject.instance(ConfigService).get_config_mysql())
    ))


def rabbitmq_connection_provider(binder) -> None:
    binder.bind_to_constructor(RabbitmqConnectionProvider, lambda: (
        RabbitmqConnectionProvider(inject.instance(ConfigService).get_config_rabbitmq())
    ))
