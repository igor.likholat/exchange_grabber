import asyncio

import aiohttp


async def read_stream(ticker: str):
    timeout = aiohttp.ClientTimeout(total=0)
    async with aiohttp.ClientSession(headers={
        'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJlNzdkYjYxMS0yMDQ1LTQ2OTAtYThkNS1mOTFiMWFhM2JiOGYiLCJpc3MiOiIyZDBjODU4Ni05ODUxLTQ0NDAtOWRmOC00ZjVhM2YyNmM0ZGIiLCJpYXQiOjE1OTYyMDA1MzcsImF1ZCI6WyJmZWVkIiwic3ltYm9scyIsIm9obGMiLCJjcm9zc3JhdGVzIl19.ALZWkyKcE94xX4jL0b6qy4GUoVjbWywkWfj70ZTENoc',
        'Accept': 'application/x-json-stream',
    }, timeout=timeout) as session:
        async with session.get('https://api-demo.exante.eu/md/2.0/feed/{}'.format(ticker)) as resp:
            while True:
                try:
                    chunk = await resp.content.readline()
                    if not chunk:
                        break
                    print(chunk)
                except asyncio.TimeoutError:
                    print("\n\n\nTIME OUT\n\n\n")


async def run():
    tickers = ['AAPL.NASDAQ', 'TSLA.NASDAQ']
    tasks = [read_stream(ticker=ticker) for ticker in tickers]
    await asyncio.gather(*tasks)


asyncio.run(run())

#
# @asyncio.coroutine
# def print_http_headers(url):
#     url = urllib.parse.urlsplit(url)
#     reader, writer = yield from asyncio.open_connection(url.hostname, 80)
#     query = ('HEAD {url.path} HTTP/1.0\r\n'
#              'Host: {url.hostname}\r\n'
#              'Accept: application/x-json-stream\r\n'
#              'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJlNzdkYjYxMS0yMDQ1LTQ2OTAtYThkNS1mOTFiMWFhM2JiOGYiLCJpc3MiOiIyZDBjODU4Ni05ODUxLTQ0NDAtOWRmOC00ZjVhM2YyNmM0ZGIiLCJpYXQiOjE1OTYyMDA1MzcsImF1ZCI6WyJmZWVkIiwic3ltYm9scyIsIm9obGMiLCJjcm9zc3JhdGVzIl19.ALZWkyKcE94xX4jL0b6qy4GUoVjbWywkWfj70ZTENoc\r\n'
#              '\r\n').format(url=url)
#     writer.write(query.encode('latin-1'))
#     while True:
#         line = yield from reader.readline()
#         if not line:
#             break
#         line = line.decode('latin1').rstrip()
#         if line:
#             print('HTTP header> %s' % line)


# url = sys.argv[1]
# loop = asyncio.get_event_loop()
# loop.run_until_complete(print_http_headers(url))
# loop.close()
