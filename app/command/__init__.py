import argparse
from config.log import config_logging
config_logging()


def parse_unknown_args(unknown):
    dynamic_parser = argparse.ArgumentParser()
    [dynamic_parser.add_argument(unknown[i]) for i in range(len(unknown)) if unknown[i].startswith("--")]
    return dynamic_parser.parse_args(unknown)


def get_args_as_dict(args) -> dict:
    return {arg: getattr(args, arg) for arg in vars(args)}


def get_all_args_as_dict(parser: argparse.ArgumentParser) -> dict:
    args, unknown = parser.parse_known_args()
    dynamic_args = parse_unknown_args(unknown)
    return {
        **get_args_as_dict(args),
        **get_args_as_dict(dynamic_args),
    }
