from App import App
from service.worker import Worker
from inject import InjectorException
from . import get_all_args_as_dict
import argparse
import sys


parser = argparse.ArgumentParser("worker command")
parser.add_argument("--name", help="Worker service name", type=str, required=True)
parser.add_argument("--min_tick_time_sec", type=float, required=False, default=1.0)
args, unknown = parser.parse_known_args()


def start() -> None:
    try:
        service = App.get_instance(True).get_service(args.name)
    except InjectorException as e:
        sys.exit('exception on getting service {}: {}'.format(args.name, e))
    if not isinstance(service, Worker):
        sys.exit('service {} is not a worker'.format(args.name))
    service.handle(**get_all_args_as_dict(parser))


if __name__ == '__main__':
    start()
